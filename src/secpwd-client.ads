package secpwd.client is

   function Request( host : string ;
                     port : integer ;
                     NumSegments : Integer := 2 ;
                     Separator : String := "-" ) return String ;

end secpwd.client;
