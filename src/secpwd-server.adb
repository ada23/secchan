with System; use System;
with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Strings.Fixed ;

with passwords ;
with words_str ;

with channel.server ;

package body secpwd.server is
 
   procedure PwdGenerator(msg : Address ; msglen : integer ;
                          rep : Address ; replen : in out integer ) is
      req : PwdReq_Type ;
      for req'Address use msg ;
      resp : PwdResp_Type ;
      for resp'Address use rep ;
   begin
      if Verbose
      then
         Put_Line("Password Generator");
      end if ;
      if msglen /= PwdReq_Type'Size/8
      then
         Put("Request Message invalid length "); Put(msglen) ; New_Line ;
         replen := 0 ;
         return ;
      end if ;
     
      declare
         sep : string := Ada.Strings.Fixed.Trim( req.Separator , Ada.Strings.Both );
      begin
         declare
            newpwd : string := passwords.Generate(words_str.words,req.NumSegments,sep) ;
         begin
            if Verbose
            then
               Put_Line(newpwd) ;
            end if ;
            Ada.Strings.Fixed.Move( newpwd , resp.pwd ) ;
         end ;
      end ;
      replen := PwdResp_Type'Size/8 ;
   end PwdGenerator ;
   
   
   procedure start( port : integer ) is
   begin
      channel.server.StartService(port,PwdGenerator'Access);
   end start ;

end secpwd.server;
