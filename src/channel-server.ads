with System; use System;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

package channel.server is

   type ServerChannel_Type is private ;
   type MessageCB is access procedure (msg : Address ; msglen : integer ;
                                       resp : Address ; resplen : in out integer ) ;
   

   procedure StartService( port : integer ;
                           cb : MessageCB );
private
   type ServerChannelRec_Type is
      record
         chan : SecureChannel_Type ;
         custno : integer := 0 ;
         custsock : GNAT.Sockets.Socket_Type; 
         custaddr : Unbounded_String := Null_Unbounded_String ;
      end record ;
   type ServerChannel_TYpe is access ServerChannelRec_TYpe ;
   task type ServiceProvider is
      entry Provide( id : integer ; 
                     client : GNAT.Sockets.Socket_Type ; 
                     cb : MessageCB ) ;
   end ServiceProvider ;
   type ServiceProviderPtr is access ServiceProvider ;   
end channel.server;
