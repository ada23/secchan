with System ; use System ;
with Interfaces ; use Interfaces ;
with Ada.Streams ; use Ada.Streams ;
with Ada.Streams.Stream_IO ; use Ada.Streams.Stream_Io;
with GNAT.Sockets ;

with sodium.pksb ;

with Interfaces ; use Interfaces ;
package channel is
   Verbose : boolean := true ;
   procedure Initialize ;   

   type SecureChannel_Type is private ;
   
   MAXMESSAGESIZE : constant := 2048 ;
   type RawMessage_Type is array (1..MAXMESSAGESIZE) of Unsigned_8 ;
   
   function Create return SecureChannel_Type ;
   function Create( from : GNAT.sockets.Socket_Type ) return SecureChannel_Type ;
   
   procedure Shutdown ( chan : SecureChannel_Type ) ;
   procedure Send( chan : SecureChannel_Type ;
                   payload : Address ; length : integer ) ;
   procedure Send( sock : GNAT.Sockets.Socket_Type ;
                   payload : Address ; length : integer ) ;
   
   procedure Receive( chan : SecureChannel_Type ;
                      payload : Address ; length : in out integer ) ;
   procedure Receive( sock : GNAT.Sockets.Socket_Type ;
                      payload : Address ;
                      buflen : integer ) ;
     
                      
private
   type SecureChannelRec_Type is
      record
         sock : GNAT.Sockets.Socket_Type ;
         mysk : sodium.pksb.Secret_KeyPtr ;          -- My Secret Key for encryption
         mypk : sodium.pksb.Public_KeyPtr ;          -- My Public Key for encryption
         ppk : sodium.pksb.Public_KeyPtr ;           -- Partner's public Key for encryption
         recsin : Integer := 0 ;
         recsout : Integer := 0 ;
         msgsize : Short_Integer := 0 ;
         sentbytes : Integer := 0 ;
         recvdbytes : Integer := 0 ;
         msgbuf : Stream_Element_Array(1..Stream_Element_Count(MAXMESSAGESIZE));
      end record;   
   type SecureChannel_Type is access SecureChannelRec_Type ;
   type PacketLength_Type is new Unsigned_16 ;
end channel;
