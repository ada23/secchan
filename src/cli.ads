with gnat.strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "secpwdgen" ;
   Verbose : aliased boolean ;

   HelpOption : aliased boolean ;
   clientOption : aliased boolean ;
   
  
   host : aliased GNAT.Strings.String_Access ;
   DEFAULT_HOST : aliased String := "127.0.0.1" ;
   
   port : aliased integer ;
   DEFAULT_PORT : Integer := 1984 ;
   
   -- Client Options
   NumSegments : aliased Integer := 2 ;   
   Separator : aliased GNAT.Strings.String_Access ;
   DEFAULT_SEPARATOR : aliased String := "-" ;
   NumPasswords : aliased Integer := 1 ;
   
   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

   function Get(prompt : string) return String ;

end cli ;
