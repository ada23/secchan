with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with Interfaces ; use Interfaces ;

package secpwd is
   verbose : boolean := false ;
   type PwdReq_Type is
      record
         NumSegments : aliased Integer ;
         Separator : String(1..4) ;
      end record ;
   type PwdResp_Type is
      record
         pwd : String(1..40) ;
      end record;
   procedure Initialize ;
end secpwd;
