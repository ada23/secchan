With System ; use System ;
With Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;

package channel.client is

   type ClientChannel_Type is private ;

   function Connect(  server : string ;
                      port : integer ) return ClientChannel_Type ;

   procedure Query( cl :  ClientChannel_Type ;
                    req : Address ;
                    reqlen : integer );
   procedure Response( cl : ClientChannel_Type ;
                       resp : Address ;
                       respbuflen : integer ;
                       resplen : out integer ) ;

   procedure Shutdown( cl : ClientChannel_Type ) ;
private
   type ClientChannelRec_Type is
      record
         chan : SecureChannel_Type ;
         server : Unbounded_String := Null_Unbounded_String ;
      end record ;
   type ClientChannel_TYpe is access ClientChannelRec_TYpe ;
end channel.client;
