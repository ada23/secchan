with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with GNAT.Sockets ;
with sodium.helpers ;

package body channel.client is

   function Connect( server : string ;
                     port : integer ) return ClientChannel_Type is
      cl : ClientChannel_Type ;
      ServerAddr : GNAT.Sockets.Sock_Addr_Type ;
      msglen : PacketLength_Type ; 
      buflen : Integer ;
   begin
      cl := new ClientChannelRec_Type ;
      cl.chan := Create ;

      ServerAddr.Addr := GNAT.Sockets.Inet_Addr(server);
      ServerAddr.Port := GNAT.Sockets.Port_Type (port);
      GNAT.Sockets.Connect_Socket(cl.chan.sock,ServerAddr) ;
      if Verbose
      then
         Put("Connected to "); Put(port); Put("@"); Put_Line(server);
      end if ;
      msglen := cl.chan.mypk.all'Size/8 ;
      Send(cl.chan.sock,msglen'Address,msglen'Size/8);
      Send(cl.chan.sock,cl.chan.mypk.all'Address,Integer(msglen));
      if Verbose
      then
         Put(">>> : my public key ");
         Put_Line(sodium.helpers.bin2hex(cl.chan.mypk.all'Address,cl.chan.mypk.all'Length));
      end if ;
      buflen := PacketLength_Type'Size/8 ;
      Receive(cl.chan.sock,msglen'Address,buflen);
      if buflen /= msglen'Size/8
      then
         raise Program_Error with "Server reply: ppk msglen" ;
      end if ;
      declare
         partnerpubkey : sodium.Block_Type(1..integer(msglen)) ;
      begin
         buflen := integer(msglen) ;
         Receive(cl.chan.sock,partnerpubkey'Address,buflen) ;
         if Verbose
         then
            Put("<<< Partner Public Key: ");
            Put_Line(sodium.helpers.bin2hex(partnerpubkey'Address,partnerpubkey'Length));
         end if ;
         
         cl.chan.ppk := new sodium.Block_Type(1..buflen) ;
         cl.chan.ppk.all := partnerpubkey ;
      end ;
      return cl ;
   end Connect ;
   
   procedure Query( cl : ClientChannel_Type ;
                    req : Address ;
                    reqlen : integer ) is
      buflen : Integer ;
   begin
      Send( cl.chan , req , reqlen ) ;
      cl.chan.recsout := cl.chan.recsout + 1 ;
      cl.chan.sentbytes := cl.chan.sentbytes + reqlen + PacketLength_Type'Size/8 ;
      buflen := cl.chan.msgbuf'Length ;
      Receive( cl.chan , cl.chan.msgbuf'Address , buflen ) ;
      cl.chan.recsin := cl.chan.recsin + 1 ;
      cl.chan.msgsize := Short_Integer(buflen) ;
      cl.chan.recvdbytes := cl.chan.recvdbytes + buflen + PacketLength_Type'Size/8 ;
   end Query ;
   procedure Response( cl : ClientChannel_Type ;
                       resp : Address ;
                       respbuflen : integer ;
                       resplen : out integer ) is
      result : Stream_Element_Array(1..Stream_ELement_Offset(respbuflen)) ;
      for result'Address use resp ;
   begin
      resplen := integer( cl.chan.msgsize ) ;
      if respbuflen <= resplen
      then
         result := cl.chan.msgbuf(1..Stream_Element_Offset(resplen));
      else
         result(1..Stream_Element_Offset(resplen)) := cl.chan.msgbuf(1..Stream_Element_Offset(resplen));
      end if ;
   end Response ;
   
   procedure Shutdown( cl : ClientChannel_Type ) is
   begin
      Shutdown(cl.chan) ;
   end Shutdown ;
   
end channel.client;
