with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_IO ;
with GNAT.Sockets ; use GNAT.Sockets ;
with sodium.crypto ; 
with sodium.pksb ;
with sodium.helpers ;

package body channel is

   procedure Initialize is
   begin
      Put_Line(sodium.Version) ;
   end Initialize ;
   
   function Create return SecureChannel_Type is
      mysock : GNAT.Sockets.Socket_Type ;
   begin
      GNAT.Sockets.Create_Socket( mysock ) ;
      return Create( mysock ) ;
   end Create ;
   function Create( from : GNAT.sockets.Socket_Type ) return SecureChannel_Type is
      result : SecureChannel_Type := new SecureChannelRec_Type ;
   begin
      result.sock := from ;
      sodium.pksb.Generate( result.mypk , result.mysk ) ;
      if Verbose
      then
         Put("Public Key : ");
         PUt_Line(sodium.helpers.bin2hex(result.mypk.all'address,result.mypk.all'Length));
         Put("Secret Key : ");
         Put_Line(sodium.helpers.bin2hex(result.mysk.all'Address,result.mysk.all'Length));
      end if ;
      return result ;
   end Create ;
   
   procedure Shutdown ( chan : SecureChannel_Type ) is
   begin
      GNAT.Sockets.Close_Socket(chan.sock) ;
      if Verbose
      then
         Put("Closing socket");
         New_Line ;
      end if ;
   end Shutdown ;
   
   procedure Send( sock : GNAT.Sockets.Socket_Type ;
                   payload : Address ; length : integer ) is
      pla : Stream_Element_Array(1..Stream_Element_Count(length));
      for pla'Address use payload ;
      sent : Stream_Element_Offset ;
      totalsent : integer := 0 ;
      tobesent : integer := 1 ;
   begin
      if Verbose
      then
         Put("==> ");
         Put_Line(sodium.helpers.bin2hex(payload,length));
      end if ;
      
      while totalsent < length
      loop
         GNAT.Sockets.Send_Socket(sock,pla(Stream_Element_Offset(tobesent)..pla'Last),sent,null);
         totalsent := totalsent + Integer(sent) ;
         tobesent := tobesent + integer(sent) ;
      end loop ;
   end Send ;
   procedure Receive( sock : GNAT.Sockets.Socket_Type ;
                      payload : Address ;
                      buflen : integer ) is
      pla : Stream_Element_Array(1..Stream_Element_Count(buflen));
      for pla'Address use payload ;
      recvd : Stream_Element_Offset ;
      totalrecvd : integer := 0 ;
      torecv : integer := 1 ;
   begin
       
      while totalrecvd < buflen
      loop
         GNAT.Sockets.Receive_Socket(sock, pla(Stream_Element_Offset(torecv)..pla'Last),recvd);
         totalrecvd := totalrecvd + Integer(recvd) ;
         torecv := torecv + integer(recvd) ;
      end loop ;
      if Verbose
      then
         Put("<== ");
         Put_Line(sodium.helpers.bin2hex(payload,buflen));
      end if ;
   end Receive ;
   
   procedure Send( chan : SecureChannel_Type ;
                   payload : Address ; length : integer ) is
      enc : sodium.crypto.EncryptedBlock_Ptr ;
      pktlenw : PacketLength_Type ;      
   begin
      --Put("Sending payload length "); Put(length); New_Line ;
      if verbose
      then
         Put("--> ");
         Put_Line(sodium.helpers.bin2hex(payload,length)) ;
      end if;
               
      enc := sodium.pksb.Seal( payload , length , chan.ppk ) ;
      pktlenw := enc.all'Length ;
      Send( chan.sock , pktlenw'Address , PacketLength_Type'Size / 8 ) ;
      Send( chan.sock , enc.all'Address , enc.all'Length ) ;
      if Verbose
      then   
         Put("-> "); Put(sodium.helpers.bin2hex(enc.all'Address,enc.all'Length)); New_Line;
      end if ;
      
   end Send ;
   procedure Receive( chan : SecureChannel_Type ;
                      payload : Address ; 
                      length : in out integer ) is
      pktlenw : PacketLength_Type ;

   begin
      Receive( chan.sock , pktlenw'Address , pktlenw'Size / 8 );
      declare
         ct : sodium.crypto.ClearText_Ptr ;
         encrypted : aliased sodium.Block_TYpe(1..integer(pktlenw));

      begin
         Receive( chan.sock , encrypted'Address , integer(pktlenw) ) ;

         ct := sodium.pksb.Unseal(encrypted'Address , integer(pktlenw) ,chan.mypk,chan.mysk );
         declare
            ctblock : aliased sodium.crypto.ClearText_Type(1..ct.all'Length) ;
            for ctblock'Address use payload ;
         begin
            ctblock := ct.all ;
         end ;
         length := ct.all'Length ;
      end ;
      if Verbose
      then
         Put("<-- "); 
         Put_Line(sodium.helpers.bin2hex(payload,length)); 
         New_Line ;
      end if ;
   end Receive ;
   
end channel;
