with Ada.Text_Io; use Ada.Text_Io;
with channel ;
with cli ;

with secpwd.client ;
with secpwd.server ;

procedure secpwdgen is
    sc : channel.SecureChannel_Type ;
begin
   cli.ProcessCommandLine ;
   channel.Verbose := cli.Verbose ;
   secpwd.verbose := cli.verbose ;

   if cli.clientOption
   then
      for n in 1..cli.NumPasswords
      loop
         declare
            pwd : string := secpwd.client.Request(cli.host.all,cli.port,cli.NumSegments,
                                                  cli.Separator.all) ;
         begin
            Put_Line(pwd);
         end ;
         delay 2.0 ;
      end loop ;

   else
      secpwd.server.Start(cli.port);
   end if ;

end secpwdgen;
