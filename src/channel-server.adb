with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with Ada.Calendar ;

with GNAT.Sockets; 
with GNAT.Calendar ;

with sodium.helpers ;
with sodium.crypto ;

package body channel.server is

   procedure StartService( port : integer ;
                           cb : MessageCB ) is
      Mysocket : GNAT.Sockets.Socket_Type;
      myaddr   : GNAT.Sockets.Sock_Addr_Type;
      
      mytask      : ServiceProviderPtr;
      clientcount : Integer := 0;
      
   begin
      GNAT.Sockets.Create_Socket (Mysocket);
      myaddr.Addr := GNAT.Sockets.Any_Inet_Addr;
      myaddr.Port := GNAT.Sockets.Port_Type (port);
      GNAT.Sockets.Bind_Socket (Mysocket, myaddr);

      loop
      GNAT.Sockets.Listen_Socket (Mysocket);
         loop
            declare
               use GNAT.Sockets;
               clientsocket : GNAT.Sockets.Socket_Type;
               clientaddr   : GNAT.Sockets.Sock_Addr_Type;
               accstatus    : GNAT.Sockets.Selector_Status;
               started : GNAT.Calendar.Hour_Number := GNAT.Calendar.Hour(Ada.Calendar.Clock);
            begin
               GNAT.Sockets.Accept_Socket
                 (Mysocket, clientsocket, clientaddr, 5.0, Status => accstatus);
               if accstatus = GNAT.Sockets.Completed then
                  clientcount := clientcount + 1;
                  if Verbose
                  then
                     Put ("New Client: ");
                     Put (GNAT.Sockets.Image (clientaddr));
                     New_Line;
                  end if ;
                  
                  mytask      := new ServiceProvider;
                  mytask.Provide (clientcount, clientsocket,cb);
               else
                  if Verbose
                  then
                     Put_Line("Server timer");
                  end if ;
                  declare
                     hnow : GNAT.Calendar.Hour_Number := GNAT.Calendar.Hour( Ada.Calendar.Clock ) ;
                  begin
                     if hnow /= started
                     then
                        started := hnow ;
                     end if ;
                  end ;
               end if;
            end;
         end loop ;
      end loop;
   end StartService ;
   
task body ServiceProvider is
   use Ada.Streams ;
      myid     : Integer;
      mychan : ServerChannelRec_Type;      

      buflen : Integer; 
      msglen : PacketLength_Type ;
      
      ctblock : sodium.crypto.ClearText_Type(1..MAXMESSAGESIZE) ;
      ctblocklen : integer ;
      
      repctblock : sodium.crypto.ClearText_Type(1..MAXMESSAGESIZE) ;
      repctblocklen : integer ;
      
      handler : MessageCB ;
   begin
      accept Provide (id : Integer; client : GNAT.Sockets.Socket_Type; 
                      cb : MessageCB ) do
         begin
            myid     := id;
            mychan.chan := Create(client) ;
            handler := cb ;
         end;
         Put_Line ("Server No " & Integer'Image (myid));
      end Provide ;
      
      buflen := PacketLength_Type'Size/8 ;
      Receive(mychan.chan.sock,msglen'Address,buflen);
      if buflen /= msglen'Size/8
      then
         raise Program_Error with "Server Connect. msglen" ;
      end if ;
      buflen := integer(msglen) ;
      if Verbose
      then   
         Put("<<< msglen "); Put(buflen); New_Line ;
      end if ;
      
      declare
         partnerpubkey : aliased sodium.Block_Type(1..buflen) ;
      begin
         Receive(mychan.chan.sock,partnerpubkey'Address,buflen) ;
         if Verbose
         then
            Put("<=== Partner public key ");
            Put_Line(sodium.helpers.bin2hex(partnerpubkey'Address,partnerpubkey'length));
         end if;
         mychan.chan.ppk := new sodium.Block_TYpe(1..buflen) ;
         mychan.chan.ppk.all := partnerpubkey ;
      end ;
             
      msglen := mychan.chan.ppk.all'Size/8 ;
      Send(mychan.chan.sock,msglen'Address,msglen'Size/8);    
      Send(mychan.chan.sock,mychan.chan.mypk.all'Address,mychan.chan.mypk.all'Length);
      if Verbose
      then
         Put("===> My public key"); 
         Put_Line(sodium.helpers.bin2hex(mychan.chan.mypk.all'Address,mychan.chan.mypk.all'Length));
      end if ;
      
      loop
         begin
            ctblocklen := ctblock'Length ;
            Receive(mychan.chan,ctblock'Address,ctblocklen) ;
            if Verbose
            then   
               Put("<-- PwdReq "); 
               Put_Line(sodium.helpers.bin2hex( ctblock'Address , ctblocklen )); 
            end if ;
            
            repctblocklen := repctblock'Length;
            handler( ctblock'Address, ctblocklen, repctblock'Address, repctblocklen ) ;
            Send(mychan.chan,repctblock'Address,repctblocklen) ;
         exception
            when others =>
               if Verbose
               then   
                  Put_Line("Secure Channel client exception");
               end if ;
               
               Shutdown(mychan.chan);
               exit ;
         end ;
      end loop ;
      
end ServiceProvider;

end channel.server;
