with System; use System;
with Ada.Strings.Fixed ; use Ada.Strings.Fixed ;
with Ada.Text_Io; use Ada.Text_Io;

with channel ;
with channel.client ;


package body secpwd.client is

   function Request( host : string ;
                     port : integer ;
                     NumSegments : Integer := 2 ;
                     Separator : String := "-" ) return String is
      clchan : channel.Client.ClientChannel_Type ;
      req : PwdReq_Type ;
      resp : PwdResp_Type ;
      resplen : integer ;
   begin
      clchan := channel.client.Connect(host,port) ;
      req.NumSegments := NumSegments ;
      Ada.Strings.Fixed.Move(separator,req.separator);
      channel.client.Query(clchan,req'Address,req'Size/8);
      channel.client.Response(clchan,resp'Address,resp'Size/8,resplen) ;
      if resplen = PwdResp_Type'Size/8
      then
         if Verbose
         then   
            Put("<<< : ");
            Put_Line(resp.pwd);
         end if ;
         
         return Ada.Strings.Fixed.Trim(resp.pwd,Ada.Strings.both);      
      end if ;
      Put_Line("Invalid Response from Server");
      channel.client.Shutdown(clchan);
      return "" ;
   end Request ;
   
end secpwd.client;
